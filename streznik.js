var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	// ...
	res.send('Potrebno je implementirati dodajanje oseb!');
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	// ...
	if(req == null){
		res.send('Napacna zahteva!');
	}
	for(var i = 0; i < uporabnikiSpomin.length; i++){
		if(req.id == uporabnikiSpomin){
			uporabnikiSpomin.splice(i, 1);
			res.redirect("https://ois-izpit-2015-02-16-columbae.c9.io/");
		}
	}
	<br>'Oseba z davčno številko ' + req.id + ' ne obstaja.'<br/><a href='javascript:window.history.back()'>Nazaj</a>
	res.send();
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];